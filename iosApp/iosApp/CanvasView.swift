//
//  CanvasView.swift
//  iosApp
//
//  Created by user191840 on 4/10/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import UIKit
import shared

class CanvasView: UIView, DrawingActions {
    var drawingHelper: DrawingHelper?
    
    init() {
        super.init(frame: CGRect.zero)
        drawingHelper = DrawingHelper(drawingActions: self as DrawingActions)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawingHelper?.draw()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else { return }
        drawingHelper?.touchStart(point: Point.init(x: Double(point.x), y: Double(point.y)))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else { return }
        drawingHelper?.touchDraw(point: Point.init(x: Double(point.x), y: Double(point.y)))
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else { return }
        drawingHelper?.touchEnd(point: Point.init(x: Double(point.x), y: Double(point.y)))
    }
    
    func setActiveColor(color: Color) {
        drawingHelper?.activeColor = color
    }
    
    func setActiveMode(mode: Mode) {
        drawingHelper?.activeMode = mode
    }
    
    func move(point: Point) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.move(to: CGPoint.init(x: point.x, y: point.y))
    }
    
    func addLine(point: Point) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.addLine(to: CGPoint.init(x: point.x, y: point.y))
    }
    
    func addRect(start: Point, end: Point) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        let rect = CGRect.init(x: start.x, y: start.y, width: end.x-start.x, height: end.y-start.y)
        context.addRect(rect)
    }
        
    func end(point: Point) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.strokePath()
    }
    
    func redraw() {
        setNeedsDisplay()
    }
    
    func setPaintColor(color: Color) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setLineWidth(10)
        context.setLineCap(.round)
        context.setStrokeColor(color.value.toNativeColor().cgColor)
    }
    
    private func getPointFromCGPoint(point: CGPoint) -> Point {
        return Point.init(x: Double(point.x), y: Double(point.y))
    }
    
    private func getCGPointFromPoint(point: Point) -> CGPoint {
        return CGPoint.init(x: point.x, y: point.y)
    }
}
