//
//  CanvasViewWrapper.swift
//  iosApp
//
//  Created by user191840 on 4/10/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct CanvasViewWrapper: UIViewRepresentable {
    var color: shared.Color
    var mode: Mode
    
    func makeUIView(context: Context) -> CanvasView {
        return CanvasView()
    }
    
    func updateUIView(_ uiView: CanvasView, context: Context) {
        uiView.backgroundColor = .white
        uiView.setActiveColor(color: color)
        uiView.setActiveMode(mode: mode)
    }
    
    func color(_ color: shared.Color) -> CanvasViewWrapper {
        var view = self
        view.color = color
        return view
    }
    
    func mode(_ mode: Mode) -> CanvasViewWrapper {
        var view = self
        view.mode = mode
        return view
    }
}
