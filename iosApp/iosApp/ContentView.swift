import SwiftUI
import shared

struct ContentView: View {
    @State var color = shared.Color.black
    @State var mode = Mode.regular

    var body: some View {
        VStack(alignment: .center) {
            CanvasViewWrapper(color: color, mode: mode).color(color).mode(mode)
            
            HStack {
                Button(action: {
                    color = shared.Color.black
                }) {
                    Circle()
                        .frame(width: 25, height: 25)
                        .padding()
                        .foregroundColor(Color.black)
                }
                
                Button(action: {
                    color = shared.Color.green
                }) {
                    Circle()
                        .frame(width: 25, height: 25)
                        .padding()
                        .foregroundColor(Color.green)
                }
                
                Button(action: {
                    color = shared.Color.red
                }) {
                    Circle()
                        .frame(width: 25, height: 25)
                        .padding()
                        .foregroundColor(Color.red)
                }
                
                Button(action: {
                    color = shared.Color.yellow
                }) {
                    Circle()
                        .frame(width: 25, height: 25)
                        .padding()
                        .foregroundColor(Color.yellow)
                }
                
                Button(action: {
                    color = shared.Color.blue
                }) {
                    Circle()
                        .frame(width: 25, height: 25)
                        .padding()
                        .foregroundColor(Color.blue)
                }
    
            }
            
            Picker("Mode", selection: $mode) {
                Image(systemName: "scribble").tag(Mode.regular)
                Image(systemName: "highlighter").tag(Mode.straight)
                Image(systemName: "rectangle").tag(Mode.rectangle)
                Image(systemName: "pencil.slash").tag(Mode.eraser)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
