package com.jetbrains.test.data

import android.graphics.Color

actual typealias NativeColor = Int

actual fun ColorValues.toNativeColor(): NativeColor = Color.rgb(red, green, blue)