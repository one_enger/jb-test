package com.jetbrains.test.data

enum class Color(val value: ColorValues) {
    BLACK(ColorValues(0, 0, 0)),
    BLUE(ColorValues(0, 122, 255)),
    GREEN(ColorValues(52, 199, 89)),
    RED(ColorValues(255, 59, 48)),
    YELLOW(ColorValues(255, 204, 0)),
    WHITE(ColorValues(255, 255, 255)),
}

class ColorValues(val red: Int, val green: Int, val blue: Int)