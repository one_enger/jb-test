package com.jetbrains.test.data

enum class Mode {
    REGULAR,
    STRAIGHT,
    RECTANGLE,
    ERASER
}