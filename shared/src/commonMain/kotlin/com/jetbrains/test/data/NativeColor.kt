package com.jetbrains.test.data

expect class NativeColor

expect fun ColorValues.toNativeColor(): NativeColor