package com.jetbrains.test.data

data class Path(val type: PathType, val point: Point, val color: Color, val secondaryPoint: Point? = null)

enum class PathType {
    MOVE,
    LINE,
    RECT,
    END,
    ERASE
}
