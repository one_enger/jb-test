package com.jetbrains.test.data

data class Point(val x: Double, val y: Double)