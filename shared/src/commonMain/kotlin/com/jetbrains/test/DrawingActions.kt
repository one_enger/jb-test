package com.jetbrains.test

import com.jetbrains.test.data.Color
import com.jetbrains.test.data.Point

interface DrawingActions {
    fun move(point: Point)

    fun addLine(point: Point)

    fun addRect(start: Point, end: Point)

    fun end(point: Point)

    fun setPaintColor(color: Color)

    fun redraw()
}