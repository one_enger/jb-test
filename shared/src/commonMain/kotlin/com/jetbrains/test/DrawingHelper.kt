package com.jetbrains.test

import com.jetbrains.test.data.*
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class DrawingHelper(private val drawingActions: DrawingActions) {
    private val paths = ArrayList<Path>()
    private var lastMovedPoint: Point? = null
    var activeMode = Mode.REGULAR
    var activeColor by Delegates.observable(Color.BLACK) { _: KProperty<*>, _: Color, colorNew: Color ->
        drawingActions.setPaintColor(colorNew)
    }

    fun draw() {
        for (path in paths) {
            if (path.type == PathType.ERASE)
                drawingActions.setPaintColor(Color.WHITE)
            else if (path.type != PathType.END && path.type != PathType.MOVE)
                drawingActions.setPaintColor(path.color)
            when (path.type) {
                PathType.LINE, PathType.ERASE -> drawingActions.addLine(path.point)
                PathType.RECT -> path.secondaryPoint?.let { drawingActions.addRect(it, path.point) }
                PathType.MOVE -> drawingActions.move(path.point)
                PathType.END -> drawingActions.end(path.point)
            }
        }
    }

    fun touchStart(point: Point) {
        paths.add(Path(PathType.MOVE, point, activeColor))
        lastMovedPoint = point
        drawingActions.redraw()
    }

    fun touchDraw(point: Point) {
        // Check if path was automatically closed
        if (paths.last().type == PathType.END) {
            paths.removeLast()
            // Save only final result when drawing straight line or rectangle
            if (activeMode == Mode.STRAIGHT || activeMode == Mode.RECTANGLE)
                paths.removeLast()
        }
        if (activeMode == Mode.RECTANGLE)
            paths.add(Path(PathType.RECT, point, activeColor, lastMovedPoint))
        else if (activeMode == Mode.ERASER)
            paths.add(Path(PathType.ERASE, point, activeColor))
        else
            paths.add(Path(PathType.LINE, point, activeColor))
        // Automatically end path to show line on screen
        paths.add(Path(PathType.END, point, activeColor))
        drawingActions.redraw()
    }

    fun touchEnd(point: Point) {
        paths.add(Path(PathType.END, point, activeColor))
        drawingActions.redraw()
    }
}