package com.jetbrains.test.data

import platform.UIKit.UIColor

@Suppress("CONFLICTING_OVERLOADS") actual typealias NativeColor = UIColor

actual fun ColorValues.toNativeColor(): NativeColor {
    return UIColor.colorWithRed(
        red = red/255.0,
        green = green/255.0,
        blue = blue/255.0,
        alpha = 1.0)
}
