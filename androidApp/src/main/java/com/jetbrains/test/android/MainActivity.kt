package com.jetbrains.test.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.jetbrains.test.android.databinding.ActivityMainBinding
import com.jetbrains.test.data.Color
import com.jetbrains.test.data.Mode

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var colorObservable = MutableLiveData<Color>().apply { value = Color.BLACK }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.apply {
            activity = this@MainActivity
            lifecycleOwner = this@MainActivity
            executePendingBindings()
        }

        binding.modes.addOnButtonCheckedListener { _, checkedId, _ ->
            val mode = when (checkedId) {
                R.id.regular -> Mode.REGULAR
                R.id.straight -> Mode.STRAIGHT
                R.id.rect -> Mode.RECTANGLE
                R.id.eraser -> Mode.ERASER
                else -> null
            }
            mode?.let { binding.drawView.setActiveMode(it) }
        }
    }

    fun setColor(color: Color) {
        colorObservable.value = color
        binding.drawView.setActiveColor(color)
    }
}
