package com.jetbrains.test.android.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.jetbrains.test.DrawingActions
import com.jetbrains.test.DrawingHelper
import com.jetbrains.test.data.Color
import com.jetbrains.test.data.Mode
import com.jetbrains.test.data.Point
import com.jetbrains.test.data.toNativeColor

class DrawView(context: Context?, attrs: AttributeSet?) : View(context, attrs), DrawingActions {
    private val drawingHelper = DrawingHelper(this)
    private lateinit var canvas: Canvas
    private lateinit var path: Path
    private var paint = Paint()

    init {
        paint.apply {
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            strokeWidth = 30f
            isAntiAlias = true
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x.toDouble()
        val y = event.y.toDouble()

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                drawingHelper.touchStart(Point(x, y))
            }
            MotionEvent.ACTION_MOVE -> {
                drawingHelper.touchDraw(Point(x, y))
            }
        }

        return true
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        this.canvas = canvas
        drawingHelper.draw()
    }

    fun setActiveColor(color: Color) {
        drawingHelper.activeColor = color
    }

    fun setActiveMode(mode: Mode) {
        drawingHelper.activeMode = mode
    }

    override fun move(point: Point) {
        path = Path()
        path.moveTo(point.x.toFloat(), point.y.toFloat())
        canvas.drawPath(path, paint)
    }

    override fun addLine(point: Point) {
        path.lineTo(point.x.toFloat(), point.y.toFloat())
        canvas.drawPath(path, paint)
    }

    override fun addRect(start: Point, end: Point) {
        canvas.drawRect(start.x.toFloat(), end.y.toFloat(), end.x.toFloat(), start.y.toFloat(), paint)
    }

    override fun end(point: Point) {
        // No actions required on Android
    }

    override fun setPaintColor(color: Color) {
        paint.color = color.value.toNativeColor()
    }

    override fun redraw() {
        invalidate()
    }
}